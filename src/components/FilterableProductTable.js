import React, {useState} from 'react'
import SearchBar from './SearchBar'
import ProductTable from './ProductTable'

// Junta as funcinalidades para exportar para o App
export default function FilterableProductTable(props) {

  const [filterText, setFilterText] = useState('');
  const [inStockOnly, setInStockOnly] = useState(false);

  return (
    <div>
      <SearchBar
        filterText={filterText}
        inStockOnly={inStockOnly}
        onFilterTextChange = { (e) => setFilterText(e) }
        onInStockChange = { (e) => setInStockOnly(e) }
      />
      <ProductTable
        products={props.products}
        filterText={filterText}
        inStockOnly={inStockOnly}
      />
    </div>
  );
}
