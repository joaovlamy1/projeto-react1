import React from 'react';

export default function SearchBar(props) {
    return (
        <form>
            {
            // Input de pesquisa dentro da tabela
            }
            <input
                type="text"
                placeholder="Search..."
                value={props.filterText}
                onChange={
                    (e) => props.onFilterTextChange(e.target.value)
                }
            />
            <p>
            {
            // Botão para tirar ou colocar items sem estoque na tabela
            }
                <input
                  type="checkbox"
                  checked={props.inStockOnly}
                  onChange={
                    (e) => props.onInStockChange(e.target.checked)
                  }
                />
                {' '}
                Only show products in stock
            </p>
        </form>
    );
}
